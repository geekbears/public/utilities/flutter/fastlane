
fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Test fastlane working
### android build
```
fastlane android build
```
Compile binary bundle
### android upload
```
fastlane android upload
```
Upload Dev to Playstore
### android bump_patch
```
fastlane android bump_patch
```
Bump version as patch
### android build_dev
```
fastlane android build_dev
```
Alias for build flavor:dev, app_identifier can be overriden in Appfile config
### android build_prod
```
fastlane android build_prod
```
Alias for build flavor:prod, app_identifier can be overriden in Appfile config
### android upload_dev
```
fastlane android upload_dev
```
Alias for upload flavor:dev, app_identifier can be overriden in Appfile config
### android upload_prod
```
fastlane android upload_prod
```
Alias for upload flavor:prod, app_identifier can be overriden in Appfile config
### android build_upload
```
fastlane android build_upload
```
Build and upload
### android build_upload_dev
```
fastlane android build_upload_dev
```
Build and upload dev
### android build_upload_prod
```
fastlane android build_upload_prod
```
Build and upload prod
### android bump_build_upload
```
fastlane android bump_build_upload
```
Bump, build and upload

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
