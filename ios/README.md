fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios test
```
fastlane ios test
```
Test fastlane working
### ios build
```
fastlane ios build
```
Compile binary archive
### ios upload
```
fastlane ios upload
```
Upload Dev to Testflight
### ios bump_patch
```
fastlane ios bump_patch
```
Bump version as patch
### ios build_dev
```
fastlane ios build_dev
```
Alias for build flavor:dev, app_identifier can be overriden in Appfile config
### ios build_prod
```
fastlane ios build_prod
```
Alias for build flavor:prod, app_identifier can be overriden in Appfile config
### ios upload_dev
```
fastlane ios upload_dev
```
Alias for upload flavor:dev, app_identifier can be overriden in Appfile config
### ios upload_prod
```
fastlane ios upload_prod
```
Alias for upload flavor:prod, app_identifier can be overriden in Appfile config
### ios build_upload
```
fastlane ios build_upload
```
Build and upload
### ios build_upload_dev
```
fastlane ios build_upload_dev
```
Build and upload dev
### ios build_upload_prod
```
fastlane ios build_upload_prod
```
Build and upload prod
### ios bump_build_upload
```
fastlane ios bump_build_upload
```
Bump, build and upload
### ios getTeamNames
```
fastlane ios getTeamNames
```

### ios match_all
```
fastlane ios match_all
```
Run Match for all identifiers
### ios match_prune_all
```
fastlane ios match_prune_all
```
Removes certs and profiles for all identifiers, (still need to work)

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
